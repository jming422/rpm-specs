%global debug_package %{nil}

Name:       rustlings
Version:    5.5.1
Release:    3%{?dist}
Summary:    Small exercises to get you used to reading and writing Rust code!

License:    MIT
URL:        https://github.com/rust-lang/rustlings
Source0:    %{url}/archive/refs/tags/%{version}.tar.gz

%if 0%{?el8}
%else
BuildRequires: cargo
BuildRequires: rust
%endif

%description
This project contains small exercises to get you used to reading and writing Rust code. This includes reading and responding to compiler messages!

%prep
%autosetup -p1
%if 0%{?el8}
  curl https://sh.rustup.rs -sSf | sh -s -- --profile minimal -y
%endif

%install
export CARGO_PROFILE_RELEASE_BUILD_OVERRIDE_OPT_LEVEL=3
%if 0%{?el8}
  $HOME/.cargo/bin/cargo build --release
%else
  cargo build --release
%endif
mkdir -p %{buildroot}%{_datadir}/rustlings
mkdir -p %{buildroot}%{_libexecdir}
mv ./exercises %{buildroot}%{_datadir}/rustlings
mv ./info.toml %{buildroot}%{_datadir}/rustlings
mv ./tests %{buildroot}%{_datadir}/rustlings
mv ./target/release/rustlings %{buildroot}%{_libexecdir}/rustlings
strip --strip-all %{buildroot}%{_libexecdir}/rustlings
mkdir -p %{buildroot}%{_datadir}/licenses/rustlings
mv ./LICENSE %{buildroot}%{_datadir}/licenses/rustlings/LICENSE
mkdir -p %{buildroot}%{_docdir}/rustlings
mv README.md %{buildroot}%{_docdir}/rustlings/README.md

mkdir -p %{buildroot}%{_bindir}
touch %{buildroot}%{_bindir}/rustlings
cat >> %{buildroot}%{_bindir}/rustlings <<EOF
#!/bin/bash
pushd %{_datadir}/rustlings
exec %{_libexecdir}/rustlings "\$@"
popd
EOF
chmod +x %{buildroot}%{_bindir}/rustlings

%files
%license LICENSE
%doc README.md
%{_bindir}/rustlings
%{_libexecdir}/rustlings
%{_datadir}/rustlings/exercises/
%{_datadir}/rustlings/tests/
%{_datadir}/rustlings/info.toml
